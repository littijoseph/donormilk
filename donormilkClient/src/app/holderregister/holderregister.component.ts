import { Component, OnInit } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { Router } from '@angular/router';
import { Holder } from '../donormilk';

@Component({
  selector: 'app-holderregister',
  templateUrl: `holderregister.component.html`,
  styles: []
})
export class HolderregisterComponent implements OnInit {

  public errorMsg: any;
  public holderName: string = '';
  public holderId: string = '';
  public holderPassword: string = '';
  public holderType: string = '';

  constructor(private router:Router, private Data:SawtoothService) { }

  ngOnInit() {
  }
  holderregister(event){
    if (this.holderName.length === 0 || !this.holderName.trim() || this.holderId.length === 0 || !this.holderId.trim() || this.holderPassword.length === 0 || !this.holderPassword.trim()) {
      this.errorMsg = "Please enter the details"
    } else {
      var newHolder = <Holder>{};
      newHolder.holderName = this.holderName;
      newHolder.holderId = this.holderId;
      newHolder.holderPassword = this.Data.hash(this.holderPassword);
      newHolder.holderType = this.holderType;

      let passPharse = this.Data.hash(this.holderId + this.holderPassword);

      this.Data.genKeyPair(passPharse.slice(0, 64));
      this.Data.addHolder(newHolder);
      this.router.navigate(['/consumer/consumerlogin']);

    }
  }
}
