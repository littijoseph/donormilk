                   
DonorMilk

Donor Milk Transparency and Traceability

We are working for improving the Donor Milk Transparency and Traceability. Currently Donor banks and NICUs do not have visibility into supply and demand across the donor milk ecosystem or a way to easily ensure that all quality controls are being met across donor banks. At a global scale, donor milk fraud and lack of traceability are known issues.We are trying to solve market problems identified in the donor milk ecosystem.

There is currently a major donor milk shortage. This has particular impact in the Neonatal Intensive Care Unit (NICU) where formula can have fatal impact on premature baby's intestines, leaving human milk to be the only viable option. At a global scale donor milk fraud, lack of traceability, and availability are known issues. While there are smaller entities engaging in the space, there is no large-scale breast milk providers and there is opportunity to expand participation from donors and accessibility for babies in need.



How DLT Tries To Overcome this Problem?

A DLT solution in this case would improve on the current solution for the following reasons:

1.  Track-and-trace,  and potential improved provenance of all milk entering the system to reduce fraud, waste, and abuse in the market/system
2. Brings transparency and visibility across different organizations like Milk Banks, Hospitals working in the space, to encourage collaboration, especially for standards creation and cost reduction practices
3. Tamper-evident Data - there is evidence of each milk donated which directs to the donor and the details about different organisations through which it got transferred before reaching the baby.
4. A distributed, decentralized tamper-evident data source can create a solution whereby there is less need for manual verification of any step that can be codified on the blockchain. If well implemented, this can lead to a less costly model than a centralized database implementation of the same model.

Components:

The project contains two parts:
   I)Client Application ,written in Angular
   II)Transaction Processor ,written in Python


I. The client application has two parts:

	* `angular application` : contains the client application that can make transactions to ther validator through REST API
	

	This angular application for DonorMilk with Donor Register/Login, Milk details entering, Consumer Register/Login, Transfer facility and also the provision to approve/reject the transfers. Also this enables us to view the list of milk assets created and currently available with each Milk Bank and end Consumer.


	Donor can:
	 1. Register into the system
	 2. Login into the system
	 3. Add the details of milk assets donated/provided
     4. Transfer the milk assets to Milk Banks

	Consumer/Milk Bank can:
	 1. Register into the system
	 2. Login into the system
	 3. Transfer the milk assets to consumers


II. The Transaction Processor in Python.

   	TransactionProcessor is having the payload class to accept the payload and consolidate into different data models, handler class - application-dependent and contains the business logic for a particular family of transactions. 
   
	The handler class is handler.py in this project


	# Brief Description about TP

	1. We have DonorMilkState class that provides the functions to perform the state updates. 

    2. Payload class to do the basic validation, verification and accept the payload

	3. handler class to call Action based routines from the state class

	4. After importing all the necessary constants we define the hashing funtions, the family name, prefix, addressing schemes, encoding and decoding functions.

	5. Then all function definitions are done. Functions for creating and updating a product by the users are defined.

	6. Next the handler for encoded payloads and the apply function is defined.

	7. Finally according to the action specified in the payload corresponding function call will be made.


	# TP Workflow


	1. The Transaction Processor works on basis of the payload it receives from the validator. 

	2. The client is where the generation of the payload is done, according to the action the user wants to do.

	3. This payload will be sent to the validator via the REST-API, which the validator passess on to the Transaction Processor.

	4. The Transaction Processor upon receiving the payload checks whether it is in correct format.

	5. Then the validater will extract the action from the payload and calls the appropriate function with the necessary variables.

	6. The functions will then be executed.
