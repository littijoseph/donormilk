Asset Definition:

There are two kinds of state entries within the DonorMilk application: Assets and Transfers. Milk, Donor and Holder are the assets.

Milk state entries are formatted as follows:

    {
        "milkBarCode": "id",
        "quantity": "qty",
        "donor": "donorid",
        "holder": "holderid",  
    }

Donor state entries are formatted as follows:

    {
        "donorName": "name",
        "donorId": "id",
        "donorAge": "age",
        "fitnessReport": "report",
        "physicianApproval": "approval",
    }

Holder state entries are formatted as follows:

    {
        "holderName": "name",
        "holderId": "id",
        "quantity": "qty",
        "holderType": "type",
    }

Transfer state entries are formatted as follows:

    {
        "milkId": "id",
        "holderId": "id",
    }

Addressing Scheme:

The address scheame used in the project is simple and efficient.  This is in such a way that at any given moment a state address can be derived based on the input value collected from the GUI. 
Following is the list of predifed address patterns used:

  DM_NAMESPACE     =   'donormilk'     [Default family name]
  DM_MILK       =   '000MILK'       [Master list for milk details]
  DM_DONOR      =   '111DONOR' 	    [Master list for donor details]
  DM_HOLDER     =   '222HOLDER'	    [Master list for holder details]
  DM_TRANSFER   =   '333TRANSFER'	[Master list for transfer details]


All the transactions have the same 6 hex digit prefix, which is the first 6 hex characters of the SHA-512 hash of "donormilk".
Each Donor & Holder is identified by with a corresponding public/private keypair which will be generated dynamically at the time of their registration.

The milk details is stored at a 70 hex digit address derived from:
* a 6-hex character prefix of the "donormilk" Transaction Family namespace and
* a 6-hex character prefix of the text "000MILK" and
* the first 58-hex characters of the milkId.

The donor details is stored at a 70 hex digit address derived from:
* a 6-hex character prefix of the "donormilk" Transaction Family namespace and
* a 6-hex character prefix of the text "111DONOR" and
* the first 58-hex characters of the donorId.

The holder details entered by the manufacture, is stored at a 70 hex digit address derived from:
* a 6-hex character prefix of the "donormilk" Transaction Family namespace and
* a 6-hex character prefix of the text "222HOLDER" and
* a 2-characters based on type(01-milkBank/02-baby) and
* the first 56-hex characters prefix of the holder unique id.

The transfer details is stored at a 70 hex digit address derived from:
* a 6-hex character prefix of the "donormilk" Transaction Family namespace and
* a 6-hex character prefix of the text "333TRANSFER" and
* a 6-hex character prefix of the holder to which transfer initiated and
* the first 52-hex characters of the milkId.
