import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderloginComponent } from './holderlogin.component';

describe('HolderloginComponent', () => {
  let component: HolderloginComponent;
  let fixture: ComponentFixture<HolderloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
