import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { createHash } from 'crypto-browserify';
import { CryptoFactory, createContext } from "sawtooth-sdk/signing";
import * as protobuf from "sawtooth-sdk/protobuf";
import { TextEncoder, TextDecoder } from "text-encoding/lib/encoding";
import { Buffer } from 'buffer/';
import { Secp256k1PrivateKey } from 'sawtooth-sdk/signing/secp256k1';
import { InvalidTransaction } from 'sawtooth-sdk/processor/exceptions';
import { Donor, Holder, Transfer } from './donormilk'
import { from } from 'rxjs';
import { DonordashboardComponent } from './donordashboard/donordashboard.component';


@Injectable({
  providedIn: 'root'
})
export class SawtoothService {

  public DM_FAMILY = 'donormilk';
  public DM_DONOR = '111DONOR';
  public DM_HOLDER = '222HOLDER';
  public DM_MILK = '000MILK';
  public DM_TRANSFER = '333TRANSFER';

  // Config variables
  readonly holderType = { 
    "milkBank": "01", "baby": "02" };

  readonly CURRDONOR = 'currentDonor';
  readonly CURRHOLDER = 'currentHolder';
  readonly MILKBANK = 'milkBank';
  readonly BABY = 'baby';
  readonly DONOR = "donor";
  readonly HOLDER = "holder";

  public DM_PASSPHARSE = 'This is my dummy passpharse for batcher key pair';

  private batchSigner: any;
  public batchPublicKey: any;
  private batchPrivateKey: any;

  private txnSigner: any;
  public txnPublicKey: any;
  private txnPrivateKey: any;

  public srcAddress: any;
  public destAddress: any;
  public transactionHeaderBytes: any;
  private context: any;


  // Current Donor
  private currentDonor: Donor;

  // Current Holder 
  private currentHolder: Holder;

  private FAMILY_VERSION = '0.0';

  private REST_API_BASE_URL = 'http://localhost:4200/api';

  private get_donor_address(donorId) {
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_DONOR).substr(0, 6) +
      this.hash(donorId).substr(0, 58)
  }

  private get_milk_address(milkBarCode) {
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_MILK).substr(0, 6) +
      this.hash(milkBarCode).substr(0, 58)
  }


  private get_holder_address(holderId, holderType) {
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_HOLDER).substr(0, 6) +
      this.holderType[holderType] +
      this.hash(holderId).substr(0, 56)
  }

  private get_transfer_address(holderId, milkBarCode) {
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_TRANSFER).substr(0, 6) +
      this.hash(holderId).substr(0, 6) +
      this.hash(milkBarCode).substr(0, 52)
  }

  private get_holder_base_address(holderType) {
    console.log("This. hlder. type:" + this.holderType[holderType])
    console.log("Holder Typeee-->" + JSON.stringify(this.holderType))
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_HOLDER).substr(0, 6) +
      this.holderType[holderType]
  }

  private get_holder_transfer_base_address(holderId) {
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_TRANSFER).substr(0, 6) +
      this.hash(holderId).substr(0, 6)
  }

  private get_milk_base_address(){
    return this.hash(this.DM_FAMILY).substr(0, 6) +
      this.hash(this.DM_MILK).substr(0, 6) 
  }




  public hash = (x) => createHash('sha512').update(x, 'utf-8').digest('hex');
  private _returnVBAddress = (type, input) => { type + this.hash(input) }


  // function to add new donor to the state
  public addDonor(donorDetails) {
    let passPharse = this.hash(donorDetails.donorId + donorDetails.donorPassword);
    this.genKeyPair(passPharse.slice(0, 64));
    this.srcAddress = []
    this.destAddress = []
    this.srcAddress.push(this.get_donor_address(donorDetails.donorId));
    this.destAddress.push(this.get_donor_address(donorDetails.donorId));
    let payload = this.getEncodedData('donorCreate', donorDetails);
    this.sendData(payload);
  }

  // function to get the donor details
  public async getDonor(donorId): Promise<any> {
    console.log("get donor called");
    let address = this.get_donor_address(donorId)
    console.log("inside getdonor>after address");
    return this.getState(address)
  }

  // function to accept milk transfer
  public async acceptMilk(transferDetails): Promise<any> {
    this.srcAddress = []
    this.destAddress = []
    //todo
    // this.srcAddress.push(this.get_donor_address(donorDetails.donorId));
    // this.destAddress.push(this.get_donor_address(donorDetails.donorId));
    this.srcAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    console.log("srcadd1:" + this.srcAddress[0]);
    this.srcAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));
    console.log("srcadd2:" + this.srcAddress[1]);
    this.destAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    this.destAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));

    console.log("descadd1:" + this.destAddress[0]);
    let payload = this.getEncodedData('acceptMilk', transferDetails);
    this.sendData(payload);
  }
  // function to accept milk transfer
  public async rejectMilk(transferDetails): Promise<any> {
    this.srcAddress = []
    this.destAddress = []
    //todo
    // this.srcAddress.push(this.get_donor_address(donorDetails.donorId));
    // this.destAddress.push(this.get_donor_address(donorDetails.donorId));
    // this.srcAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    // console.log("srcadd1:" + this.srcAddress[0]);
    this.srcAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));
    console.log("srcadd2:" + this.srcAddress[0]);
    // this.destAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    this.destAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));

    console.log("descadd1:" + this.destAddress[0]);
    let payload = this.getEncodedData('rejectMilk', transferDetails);
    this.sendData(payload);
  }
  
  public async transferMilk(transferDetails): Promise<any> {
    //let passPharse = this.hash(this.currentDonor.donorId + this.currentDonor.donorPassword);
    //this.genKeyPair(passPharse.slice(0, 64));
    this.srcAddress = []
    this.destAddress = []
    this.srcAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    console.log("srcadd1:" + this.srcAddress[0]);
    this.srcAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));
    console.log("srcadd2:" + this.srcAddress[1]);
    this.destAddress.push(this.get_milk_address(transferDetails.milkBarCode));
    this.destAddress.push(this.get_transfer_address(transferDetails.holderId, transferDetails.milkBarCode));

     console.log("descadd1:" + this.destAddress[0]); 
     console.log("descadd2:" + this.destAddress[1]);

    let payload = [];
    payload.push(this.getEncodedData('transferMilk', transferDetails));

    this.sendData(payload);

  }

  
  public async createMilk(milkDetails): Promise<any> {
    let passPharse = this.hash(this.currentDonor.donorId + this.currentDonor.donorPassword);
    this.genKeyPair(passPharse.slice(0, 64));
    this.srcAddress = []
    this.destAddress = []
    this.srcAddress.push(this.get_milk_address(milkDetails.milkBarCode));
    // console.log("srcadd1:" + this.srcAddress[0]);
    //this.srcAddress.push(this.get_transfer_address(milkDetails.holderId, milkDetails.milkBarCode));
    // console.log("srcadd2:" + this.srcAddress[1]);
    this.destAddress.push(this.get_milk_address(milkDetails.milkBarCode));
    //this.destAddress.push(this.get_transfer_address(milkDetails.holderId, milkDetails.milkBarCode));

    // console.log("descadd1:" + this.destAddress[0]);

    //let concatenatedAdd=this.srcAddress.concat()
    //console.log("Concatenated address:-->" +concatenatedAdd)
    // var newTransfer = <Transfer>{};
    // newTransfer.holderId = milkDetails.holderId;
    // newTransfer.milkBarCode = milkDetails.milkBarCode;

    //changing the milkdetails holderId back to donor id
    milkDetails.holderId = this.currentDonor.donorId;

    let payload = [];
    payload.push(this.getEncodedData('createMilk', milkDetails));
    //payload.push(this.getEncodedData('transferMilk', newTransfer));

    this.sendData(payload);


  }

 
  public async getFullHolderList(holderType): Promise<any> {
    console.log("Holder type: "+holderType)
    let address = this.get_holder_base_address(holderType);
    console.log("inside getdonor>after address");
    return this.getCompleteState(address)
  }



  public async getHolderFullTransferList(): Promise<any> {
    let address = this.get_holder_transfer_base_address(this.currentHolder.holderId);
    console.log("inside getHolderFullTransferList>after address");
    return this.getCompleteState(address)
  }

  //function to get full milk list
  public async getMilkList():Promise<any>{
   let address = this.get_milk_base_address();
   return this.getCompleteState(address)
  }

  // function to add new donor to the state
  public addHolder(holderDetails) {
    this.srcAddress = []
    this.destAddress = []
    this.srcAddress.push(this.get_holder_address(holderDetails.holderId, holderDetails.holderType));
    this.destAddress.push(this.get_holder_address(holderDetails.holderId, holderDetails.holderType));
    console.log("inside addholder-->Source address-->" + this.srcAddress)
    let payload = this.getEncodedData('holderCreate', holderDetails);
    this.sendData(payload);
  }

  // function to get the donor details
  public async getHolder(holder): Promise<any> {
    console.log("get holder called");
    let address = this.get_holder_address(holder.holderId, holder.holderType)
    console.log("inside getholder>after address");
    return this.getState(address)
  }

  // setter for donor
  public setCurrentDonor(Donor) {
    console.log("data saved to localstorage:" + JSON.stringify(Donor));
    localStorage.setItem(this.CURRDONOR, JSON.stringify(Donor))
    this.currentDonor = Donor;
  }

  // getter for donor
  public getCurrentDonor() {
    if (this.currentDonor)
      return this.currentDonor;
    else if (localStorage.getItem(this.CURRDONOR)) {
      console.log("data fetched from localstorage:" + JSON.stringify(localStorage.getItem(this.CURRDONOR)));
      this.currentDonor = JSON.parse(localStorage.getItem(this.CURRDONOR));
      return this.currentDonor
    }
    else
      return null
  }

  // setter for holder
  public setCurrentHolder(Holder) {
    console.log("data saved to localstorage:" + JSON.stringify(Holder));
    localStorage.setItem(this.CURRHOLDER, JSON.stringify(Holder))
    this.currentHolder = Holder;
  }

  // getter for holder
  public getCurrentHolder() {
    if (this.currentHolder)
      return this.currentHolder;
    else if (localStorage.getItem(this.CURRHOLDER)) {
      console.log("data fetched from localstorage:" + JSON.stringify(localStorage.getItem(this.CURRHOLDER)));
      this.currentHolder = JSON.parse(localStorage.getItem(this.CURRHOLDER));
      return this.currentHolder
    }
    else
      return null
  }

  constructor(public http: HttpClient) {
    this.context = createContext('secp256k1');
    let passPharse = this.hash(this.DM_PASSPHARSE).slice(0, 64);
    this.batchPrivateKey = Secp256k1PrivateKey.fromHex(passPharse);
    this.batchSigner = new CryptoFactory(this.context).newSigner(this.batchPrivateKey);
    this.batchPublicKey = this.batchSigner.getPublicKey().asHex();
    console.log("Batch public key : " + this.batchPublicKey);
  }

  public genKeyPair(keyData) {
    console.log(keyData);
    this.txnPrivateKey = Secp256k1PrivateKey.fromHex(keyData);
    this.txnSigner = new CryptoFactory(this.context).newSigner(this.txnPrivateKey);
    this.txnPublicKey = this.txnSigner.getPublicKey().asHex();
    console.log("Txn public key : " + this.txnPublicKey);
  }

  /******************************************************************************************** */
  private getTransaction(transactionHeaderBytes, payloadBytes): any {
    const transaction = protobuf.Transaction.create({
      header: transactionHeaderBytes,
      headerSignature: this.txnSigner.sign(transactionHeaderBytes),
      payload: payloadBytes
    });

    return transaction;
  }

  /*-------------Creating transactions & batches--------------------*/
  private getTransactionsList(payload): any {
    // Create transaction header
    const transactionHeader = this.getTransactionHeaderBytes(this.srcAddress, this.destAddress, payload);
    let transaction = this.getTransaction(transactionHeader, payload);
    let transactionsList = [transaction];
    return transactionsList;
  }

  /* Create batch list */
  private getBatchList(transactionsList): any {
    // List of transaction signatures
    const transactionSignatureList = transactionsList.map((tx) => tx.headerSignature);
    // Create batch header
    const batchHeader = this.getBatchHeaderBytes(transactionSignatureList);
    // Create the batch
    const batch = this.getBatch(batchHeader, transactionsList);
    // Batch List
    const batchList = this.getBatchListBytes([batch]);
    return batchList;
  }

  /* Batch Header*/
  private getBatchHeaderBytes(transactionSignaturesList): any {
    const batchHeader = protobuf.BatchHeader.encode({
      signerPublicKey: this.batchPublicKey,
      transactionIds: transactionSignaturesList
    }).finish();

    return batchHeader;
  }

  /* Get Batch */
  private getBatch(batchHeaderBytes, transactionsList): any {
    const batch = protobuf.Batch.create({
      header: batchHeaderBytes,
      headerSignature: this.batchSigner.sign(batchHeaderBytes),
      transactions: transactionsList
    });

    return batch;
  }

  /* Encode the payload */
  private getEncodedData(action, values): any {

    var payload = {
      'action': action,
      'asset': values
    }
    console.log(JSON.stringify(payload));

    //const data = action + "|" + values;
    // console.log(" getEncodedData: " + JSON.stringify(payload));
    // console.log(" getEncodedData after encoding: " + JSON.stringify(new TextEncoder('utf8').encode(payload)));
    return new TextEncoder('utf8').encode(JSON.stringify(payload));
  }


  private getDecodedData(responseJSON): string {
    const dataBytes = responseJSON.data;
    console.log(dataBytes)
    const decodedData = new Buffer(dataBytes, 'base64').toString();
    return decodedData;
  }

  private getBatchListBytes(batchesList): any {
    const batchListBytes = protobuf.BatchList.encode({
      batches: batchesList
    }).finish();

    return batchListBytes;
  }

  public async sendData(payload) {
    var transactionsList = new Array();
    var batchList;

    // Encode the payload
    if (payload instanceof Array) {
      payload.forEach(element => {
        console.log("inside sendData");
        console.log(JSON.stringify(element));
        let tempArray = this.getTransactionsList(element);
        transactionsList = transactionsList.concat(tempArray);
      });
      console.log("txnlist:" + JSON.stringify(transactionsList));
      // this.destAddress = this.srcAddress;

      // var name = new Array('UpdateMedicine','AddPatient');
      // var values = new Array("Inactive",newPatient);
      // this.sendData(name,values);

      //   payload = this.getEncodedData(action[0], JSON.stringify(values[0]));
      //   const tempArray = this.getTransactionsList(payload);

      //   //TODO
      //   this.srcAddress = this.destAddress;
      //   payload = this.getEncodedData(action[1], JSON.stringify(values[1]));
      //   const tempArray1 = this.getTransactionsList(payload);
      //   transactionsList = tempArray.concat(tempArray1);
    }
    else {
      console.log("else part called");

      //this.destAddress = this.srcAddress;
      // payload = this.getEncodedData(action, values);
      console.log(JSON.stringify(payload));
      transactionsList = this.getTransactionsList(payload);
    }

    batchList = this.getBatchList(transactionsList);

    // Send the batch to REST API
    await this.sendToRestAPI(batchList)
      .then((resp) => {
        console.log("Response from API sendToRestAPI ", resp);
        let respCode = resp["status"]
        if (respCode != null) {
          if (parseInt(respCode) > 300)
            alert("Previous transaction was rejected with reason[" + resp["statusText"] + "]")
        }
        return resp;
      })
      .catch((error) => {
        console.log("error here", error);
      })
  }

  public async sendToRestAPI(batchListBytes): Promise<any> {

    return this.postBatchList(batchListBytes)
      .then((response) => response.json())
      .then((responseJson) => {
        console.log("Response from API -- inside sendToRestAPI function ", JSON.stringify(responseJson));
        if (responseJson.data) { console.log(" response data ") }
        if (responseJson.error) { alert(responseJson.error.message) }
        if (responseJson.link) { console.log(" response link ") }
        //var data = responseJson.data;
        //var value = new Buffer(data, 'base64').toString();
        //return value;
        return responseJson;
      })
      .catch((error) => {
        alert("Unexpected error while processing your transaction");
        throw new InvalidTransaction("Unexpectded Error");
      });
  }


  //Get complete state 
  //TODO change to private 
  public async getCompleteState(address): Promise<any> {
    const getStateURL = this.REST_API_BASE_URL + '/state?address=' + address;
    console.log("Getting from: " + getStateURL);

    return fetch(getStateURL, {
      method: 'GET',
    })
      .then((response) => response.json())
      .then((responseJson) => responseJson.data)
      .then((responseData) => {
        console.log("responsedata:" + JSON.stringify(responseData));
        return responseData.reduce((processed, datum) => {
          if (datum.data !== '') {
            const parsed = JSON.parse(atob(datum.data))
            console.log("parsed:" + JSON.stringify(parsed));
            processed.assets.push(parsed)
          }
          return processed
        }, { assets: [] })
      })
      .catch((error) => {
        alert("Unexpected error while fetching the state");
        throw new InvalidTransaction("Unexpectded Error");
      });
  }



  //Get the state 
  //TODO change to private 
  public async getState(address): Promise<any> {
    const getStateURL = this.REST_API_BASE_URL + '/state/' + address;
    console.log("Getting from: " + getStateURL);

    return fetch(getStateURL, {
      method: 'GET',
    })
      .then((response) => response.json())
      .then((responseJson) => {
        var data = responseJson.data;
        var value = new Buffer(data, 'base64').toString();
        return value;
      })
      .catch((error) => {
        alert("Unexpected error while fetching the state");
        throw new InvalidTransaction("Unexpectded Error");
      });
  }

  //send the tranaction
  private postBatchList(batchListBytes): Promise<any> {
    const postBatchListURL = this.REST_API_BASE_URL + '/batches';
    const fetchOptions = {
      method: 'POST',
      body: batchListBytes,
      headers: {
        'Content-Type': 'application/octet-stream'
      }
    }
    return window.fetch(postBatchListURL, fetchOptions);
  }

  private getTransactionHeaderBytes(inputAddressList, outputAddressList, payload): any {
    const transactionHeaderBytes = protobuf.TransactionHeader.encode({
      familyName: this.DM_FAMILY,
      familyVersion: this.FAMILY_VERSION,
      inputs: inputAddressList,
      outputs: outputAddressList,
      signerPublicKey: this.txnPublicKey,
      batcherPublicKey: this.batchPublicKey,
      dependencies: [],
      payloadSha512: this.hash(payload),
      nonce: (Math.random() * 1000).toString()
    }).finish();

    return transactionHeaderBytes;
  }


}