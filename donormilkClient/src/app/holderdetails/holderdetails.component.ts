import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { SawtoothService } from '../sawtooth.service';
import { Transfer } from '../donormilk';

@Component({
  selector: 'app-holderdetails',
  templateUrl: `holderdetails.component.html`,
  styles: []
})
export class HolderdetailsComponent implements OnInit {
  public holder: any;
  public holderTransferGroup: string[];
  public selectedMilkBarCode: string;
  public transferErrorMsg: string;
  public selectedHolderId: string;
  public holderGroup: string[];
  public showTransfer: boolean;
  public fullMilkList: string[];

  constructor(private router: Router, private Data: SawtoothService) {
    this.holderTransferGroup = new Array;
    this.holderGroup = new Array;
    this.fullMilkList=new Array;
  }

  ngOnInit() {

    

    console.log("Current holder Details:" + JSON.stringify(this.holder));
    if (this.Data.getCurrentHolder() != null)
      this.holder = this.Data.getCurrentHolder();
    console.log("Current Donor Details:" + JSON.stringify(this.holder));
    let passPharse = this.Data.hash(this.holder.holderId + this.holder.holderPassword);
    this.Data.genKeyPair(passPharse.slice(0, 64));
    this.Data.getFullHolderList(this.Data.BABY).then((holderList) => {
      holderList.assets.forEach((holder) => {
        this.holderGroup.push(holder)
      })
    })
    this.refreshDonorTransferList();
    this.checkHolderType();
    this.Data.getMilkList().then((fullMilkList) => {
      fullMilkList.assets.forEach(milk => {
        console.log("string inside milkdetails fetched:" + JSON.stringify(milk));
        if (milk.owner == this.Data.txnPublicKey)
          this.fullMilkList.push(milk.milk)
      })
    })
  }

  acceptmilk(event, index) {

    console.log("acceptmilk called, index:" + index);
    let transfer = this.holderTransferGroup[index];
    console.log('selected transfer details:' + JSON.stringify(transfer));

    this.Data.acceptMilk(transfer);
    this.refreshDonorTransferList();

  }
  rejectmilk(event, index) {
    console.log("rejectmilk called, index:" + index);
    let transfer = this.holderTransferGroup[index];
    console.log('selected transfer details:' + JSON.stringify(transfer));


    this.Data.rejectMilk(transfer);
    this.refreshDonorTransferList();

  }
  refreshDonorTransferList() {

    this.holderTransferGroup = []
    this.Data.getHolderFullTransferList().then((fullHolderTransferList) => {
      console.log("string inside donordetails fetched:" + JSON.stringify(fullHolderTransferList.assets));
      // Populate asset views
      fullHolderTransferList.assets.forEach(transfer => {
        this.holderTransferGroup.push(transfer)
      })
      // // Populate transfer list for selected user
      // this.holderTransferGroup.forEach(
      //   transfer => this.addAction('#transferList', transfer.milkBarCode, 'Accept'))

    })
  }
  transfermilk(event) {
    //check the details
    if (this.selectedMilkBarCode.length === 0 || !this.selectedMilkBarCode.trim().toString() || this.selectedHolderId.length === 0 || !this.selectedHolderId.trim().toString()) {
      this.transferErrorMsg = "Please correct the details"
    } else {
      var newTransfer = <Transfer>{};
      newTransfer.milkBarCode = this.selectedMilkBarCode;
      newTransfer.holderId = this.selectedHolderId;

      console.log("transfer Details:" + JSON.stringify(newTransfer));

      this.Data.transferMilk(newTransfer);
      this.router.navigate(['/consumerdashboard/consumerdetails']);

    }
  }
  checkHolderType() {
    if (this.holder.holderType == this.Data.BABY) {
      this.showTransfer = false;
    }
    else {
      this.showTransfer = true;
    }
  }
}
