import { Component, OnInit } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { Router } from '@angular/router';
import { createHash } from 'crypto-browserify';
import { Buffer } from 'buffer/';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { HolderLogin } from '../donormilk';

@Component({
  selector: 'app-holderlogin',
  templateUrl: `./holderlogin.component.html`,
  styles: []
})
export class HolderloginComponent implements OnInit {

  public errorMsg: any;
  public myfield_control: any;
  public holderId: string = '';
  public holderType: string ='';
  public holderPassword: string = '';

  constructor(private router:Router, private Data:SawtoothService) { }

  ngOnInit() {
  }
  holderlogin(event) {
    //check the details
    if (this.holderId.length === 0 || !this.holderId.trim().toString() || this.holderPassword.length === 0 || !this.holderPassword.trim() || this.holderType.length === 0 || !this.holderType.trim()) {
      this.errorMsg = "Error in User Name or Password or Please select the consumer type"
    } else {
      var newLogin = <HolderLogin>{};
      newLogin.holderId = this.holderId;
      newLogin.password = this.Data.hash(this.holderPassword);
      newLogin.holderType = this.holderType;

      console.log("holderLogincomponent-->holderlogin:"+JSON.stringify(newLogin));
      

      let holderData = this.Data.getHolder(newLogin)


      holderData.then((string) => {
        const holderDataJSON = JSON.parse(string)
        console.log("holderDataJSON:" + JSON.stringify(holderDataJSON));
        let password = holderDataJSON["holderPassword"];
        console.log("password:" + password)
        if (newLogin.password === password) {
          let passPharse = this.Data.hash(this.holderId + this.holderPassword);
          this.Data.genKeyPair(passPharse.slice(0, 64));
          this.Data.setCurrentHolder(holderDataJSON);
          console.log("After set current user");
          this.router.navigate(['/consumerdashboard/consumerdetails']);
        }
        this.errorMsg = "Error in User Name or Password";
      })
    }
  }

}
