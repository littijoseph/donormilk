import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderhomeComponent } from './holderhome.component';

describe('HolderhomeComponent', () => {
  let component: HolderhomeComponent;
  let fixture: ComponentFixture<HolderhomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HolderhomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
