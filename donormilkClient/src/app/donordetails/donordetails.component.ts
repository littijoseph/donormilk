import { Component, OnInit } from '@angular/core';
import { SawtoothService } from '../sawtooth.service';
import { Router } from '@angular/router';
import { Donor, Holder, Milk, Transfer } from '../donormilk';

@Component({
  selector: 'app-donordetails',
  templateUrl: `./donordetails.component.html`,
  styles: []
})
export class DonordetailsComponent implements OnInit {

  public donateErrorMsg: any;
  public transferErrorMsg: any;
  public donor: any;
  public holderGroup: string[];
  public milkBarCode: string;
  public quantity: number;
  public selectedHolderId: string;
  public selectedMilkBarCode: string;
  public fullMilkList: string[];

  constructor(private router: Router, private Data: SawtoothService) {
    console.log("DonorDetails-->Constructor called");
    this.holderGroup = new Array;
    this.fullMilkList = new Array;
  }

  ngOnInit() {

    if (this.Data.getCurrentDonor() != null)
      this.donor = this.Data.getCurrentDonor();
    console.log("Current Donor Details:" + JSON.stringify(this.donor));
    let passPharse = this.Data.hash(this.donor.donorId + this.donor.donorPassword);
    this.Data.genKeyPair(passPharse.slice(0, 64));
    this.Data.getFullHolderList(this.Data.MILKBANK).then((fullHolderList) => {
      console.log("string inside donordetails fetched:" + JSON.stringify(fullHolderList.assets));
      // Populate asset views
      fullHolderList.assets.forEach(holder => {
        this.holderGroup.push(holder)
      })
    })

    this.Data.getMilkList().then((fullMilkList)=>{
      fullMilkList.assets.forEach(milk=>{
        console.log("string inside milkdetails fetched:" + JSON.stringify(milk));
        if (milk.owner==this.Data.txnPublicKey)
          this.fullMilkList.push(milk.milk)
      })
    })
  }

  donatemilk(event) {
    //check the details
    if (this.milkBarCode.length === 0 || !this.milkBarCode.trim().toString() || this.quantity === 0 ) {
      this.donateErrorMsg = "Please correct the details"
    } else {
      var newMilk = <Milk>{};
      newMilk.milkBarCode = this.milkBarCode;
      newMilk.quantity = this.quantity;
      newMilk.donorId = this.donor.donorId;
      console.log("milk Details:" + JSON.stringify(newMilk));

      this.Data.createMilk(newMilk);
      this.router.navigate(['/donordashboard/donordetails']);

    }
  }

  transfermilk(event) {
    //check the details
    if (this.selectedMilkBarCode.length === 0 || !this.selectedMilkBarCode.trim().toString() || this.selectedHolderId.length === 0 || !this.selectedHolderId.trim().toString()) {
      this.transferErrorMsg = "Please correct the details"
    } else {
      var newTransfer = <Transfer>{};
      newTransfer.milkBarCode = this.selectedMilkBarCode;
      newTransfer.holderId = this.selectedHolderId;

      console.log("transfer Details:" + JSON.stringify(newTransfer));

      this.Data.transferMilk(newTransfer);
      this.router.navigate(['/donordashboard/donordetails']);

    }
  }
}
