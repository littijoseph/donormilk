# Copyright 2018 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# -----------------------------------------------------------------------------

import logging

from sawtooth_sdk.processor.handler import TransactionHandler
from sawtooth_sdk.processor.exceptions import InvalidTransaction

from donormilk_processor.donormilk_payload import DonorMilkPayload
from donormilk_processor.donormilk_state import DonorMilkState
from donormilk_processor.donormilk_state import DM_NAMESPACE


LOGGER = logging.getLogger(__name__)


class DonorMilkTransactionHandler(TransactionHandler):

    @property
    def family_name(self):
        return 'donormilk'

    @property
    def family_versions(self):
        return ['0.0']

    @property
    def encodings(self):
        return ['application/json']

    @property
    def namespaces(self):
        return [DM_NAMESPACE]

    def apply(self, transaction, context):
        header = transaction.header
        signer = header.signer_public_key

        payload = DonorMilkPayload(transaction.payload)
        state = DonorMilkState(context)

        LOGGER.debug('Handling transaction: %s > %s ',
                     payload.action,
                     payload.asset,
                     #  payload.donor,
                     #  payload.holder,
                     #  payload.milk,
                     #  payload.transfer,
                     )

        if payload.action == 'donorCreate':
            LOGGER.debug('donorCreate called')
            _create_donor(donor=payload.donor,
                          owner=signer,
                          state=state)

        elif payload.action == 'holderCreate':
            _create_holder(holder=payload.holder,
                           owner=signer,
                           state=state)

        elif payload.action == 'createMilk':
            _donate_milk(milk=payload.milk,
                         owner=signer,
                         state=state)

        elif payload.action == 'transferMilk':
            _transfer_milk(transfer=payload.transfer,
                           owner=signer,
                           state=state)

        elif payload.action == 'acceptMilk':
            _accept_milk(transfer=payload.transfer,
                           owner=signer,
                           state=state)

        elif payload.action == 'rejectMilk':
            _reject_milk(transfer=payload.transfer,
                           owner=signer,
                           state=state)

        else:
            raise InvalidTransaction('Unhandled action: {}'.format(
                payload.action))


def _create_donor(donor, owner, state):
    LOGGER.debug('_create_donor called: %s', donor)
    if state.get_donor(donor) is not None:
        raise InvalidTransaction(
            'Invalid action: Donor already exist: {}'.format(donor))
    state.set_donor(donor, owner)


def _create_holder(holder, owner, state):
    if state.get_holder(holder) is not None:
        raise InvalidTransaction(
            'Invalid action: Holder already exist: {}'.format(holder))
    state.set_holder(holder, owner)


def _donate_milk(milk, owner, state):
    if state.get_milk(milk.get('milkBarCode')) is not None:
        raise InvalidTransaction(
            'Invalid action: Holder already exist: {}'.format(milk))
    state.set_milk(milk, owner)


def _transfer_milk(transfer, owner, state):
    milk_data = state.get_milk(transfer.get('milkBarCode'))
    if milk_data is None:
        raise InvalidTransaction('Milk Asset does not exist')

    if owner != milk_data.get('owner'):
        raise InvalidTransaction('Only an Asset\'s owner may transfer it')

    if state.get_transfer(transfer) is not None:
        raise InvalidTransaction(
            'Invalid action: Transfer already exist: {}'.format(transfer))

    state.set_transfer(transfer, owner)


def _accept_milk(transfer, owner, state):
    transfer_data = state.get_transfer(transfer)
    if transfer_data is None:
        raise InvalidTransaction(' Milk Asset is not being transfered')
    milk_data=state.get_milk(transfer_data.get('milkBarCode'))
    milk=milk_data.get('milk')
    state.set_milk(milk,owner)
    state.delete_transfer(transfer)


def _reject_milk(transfer, owner, state):
    transfer_data = state.get_transfer(transfer)
    if transfer_data is None:
        raise InvalidTransaction('Milk Asset is not being transfered')
    state.delete_transfer(transfer)
