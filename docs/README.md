# Getting started

Get the code from:

`git clone https://gitlab.com/littijoseph/donormilk.git`

`cd donorMilkClient`

`npm install`

`ng build`

After successfull build, you should see a donormilk folder inside the dist folder

**NOTE**
The project is built on mac os. There are chances for changes in the nginx configuration files for macos and linux around the locations specified within the conf file. Hence providing 2 conf files.

**MACOS**
For MacOS use nginx_mac.conf

if nginx not installed on mac?

`sudo brew nginx`
    
    After successfull install, replace the nginx.conf file in /etc/nginx/nginx.conf with the one provided after renaming.

(`sudo cp nginx.conf /usr/local/etc/nginx/nginx.conf`)

    Stop and start again nginx using:
    
 (`sudo nginx -s stop`
    `sudo nginx`)

(if the port is still in use:
     get PID using `sudo lsof -i:4200`
    Do `sudo kill -9 <PID>` and then do the restart
)


**LINUX**
For Linux use nginx_linux.conf

if nginx not installed on linux?

`apt-get install nginx`

    After successfull install, replace the nginx.conf file in /etc/nginx/nginx.conf with the one provided after renaming.

(`sudo cp nginx.conf /etc/nginx/nginx.conf`)

    Restart nginx using `/etc/init.d/nginx restart`
    
(if the port is still in use:
     get PID using `sudo netstat -tulpn`
    Do `sudo kill -2 <PID>` and then do the restart
)

Open appropriate nginx conf file in the repository, replace '<donormilk_proj_dist_path>' with this folder path


`sudo docker-compose up`

The application should be available at `http://localhost:4200`

Running `node subscriptionProxy.js` will start the event subscription

## Introduction

Project Name : Donor Milk

Team Members : Litti Tom

This project invloves sawtooth implementation of the MVP usecase under the project Donor Milk Transparency and Traceability (https://wiki.hyperledger.org/groups/healthcare/donor_milk_transparency_and_traceability) of The Hyperledger Healthcare Special Interest Group https://wiki.hyperledger.org/groups/healthcare/healthcare-sig


## Components
The donormilk transaction family contains two parts :

	I)Client Application ,written in Angular
	II)Transaction Processor ,written in JavaScript

1. The client application has two parts:

* `angular application` : contains the client application that can make transactions to ther validator through REST API
* `node subscription service` : A node service that subscribes to events and listens.


2. The Transaction Processor in javascript.

   TransactionProcessor is a generic class for communicating with a validator and routing transaction processing requests to a registered handler. 
   index.js has the Transaction Processor class.

   
   The handler class is application-dependent and contains the business logic for a particular family of transactions. 
   DonorMilkHandler.js has the handler class.

   The javascript transaction processor has the following files :
    
                a)index.js  (transaction processor class)
                b)package.json
                c)Dockerfile
		d)DonorMilkHandler.js (handler class)

   Files which needs modification
                
                a)docker-compose.yaml -the name of the transaction processor needs to be specified.
                b)Dockerfile-the working directory of the tp needs to be specified.

   Files newly included
                a)package.json
                b)index.js
                c)DonorMilkHandler.js
       
## Docker Usage
### Prerequisites
This example uses docker-compose and Docker containers. If you do not have these installed please follow the instructions here: https://docs.docker.com/install/


### Building Docker containers

Before starting  the project make sure the Docker service is up and running.

To start up the environment, perform the following tasks:

    a)Open a terminal window.
    b)Change your working directory to the same directory where you saved the Docker Compose file.
    c)Run the following command:


	$sudo docker-compose up --build

	The `docker-compose.yaml` file creates a genesis block, which contain initial Sawtooth settings, generates Sawtooth and client keys, 
	and starts the Validator, Settings TP, DonorMilk TP, and REST API.


To stop the validator and destroy the containers, type `^c` in the docker-compose window, wait for it to stop, then type

	$sudo docker-compose down


